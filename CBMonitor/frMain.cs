﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using System.Xml.Linq;

namespace CBMonitor
{
	public partial class frMain : Form
	{
		XDocument cb_course;

		public frMain()
		{
			InitializeComponent();
		}
		//загрузка курса валют из сайта цб,запись актуальных курсов в бд "Курс" и вывод на экран этой таблицы
		private void frMain_Load(object sender, EventArgs e)
		{
			DownloadCourse();
			InsertTodayValutes();

			SqlConnection conn = DbConnector.GetConnection();
			using (conn)
			{
				SqlCommand command = new SqlCommand("SELECT * FROM Курс;", conn);
				conn.Open();
				DataTable dt = new DataTable();
				SqlDataReader reader = command.ExecuteReader();
				dt.Load(reader);
				dgvTest.DataSource = dt;
			}
		}

		// Получение курса на определенный день из бд таблицы "Курс" с помощью sql функции
		public decimal GetCourseFromDay(DateTime date, string valute)
		{
			SqlConnection conn = DbConnector.GetConnection();
			using (conn)
			{
				SqlCommand command = new SqlCommand("SELECT dbo.GetValue(@C_DATE, @VALUTE);", conn);
				command.Parameters.Add(new SqlParameter("@C_DATE", date.ToShortDateString()));
				command.Parameters.Add(new SqlParameter("@VALUTE", valute));
				conn.Open();
				decimal result = (decimal)command.ExecuteScalar();
				return result;
			}
		}


		/// <summary>
		/// Запись в таблицу бд "Курс" всех валют которые имеются в таблице "Валюты"
		/// </summary>
		private void InsertTodayValutes()
		{
			SqlConnection conn = DbConnector.GetConnection();
			using (conn)
			{
				SqlCommand command = new SqlCommand("SELECT валюта, кодвалюты FROM Валюты;", conn);
				conn.Open();
				DataTable dt = new DataTable();
				SqlDataReader reader = command.ExecuteReader();
				dt.Load(reader);
				DateTime current_date = DateTime.Today;
				
				foreach (DataRow row in dt.Rows)
				{		
					string valute = row[0].ToString();
					string id_valute = row[1].ToString().Trim();
					decimal value = GetValueNominal(current_date, id_valute);
					command = new SqlCommand("SELECT * FROM Курс WHERE дата = @date AND валюта = @valute", conn);
					command.Parameters.AddWithValue("@date", current_date);
					command.Parameters.AddWithValue("@valute", valute);
					command.Parameters.AddWithValue("@value", value);
					reader = command.ExecuteReader();
					if (!reader.HasRows)
					{
						command.CommandText = "INSERT INTO Курс (дата, валюта, курс) VALUES (@date, @valute, @value)";
					}
					else
					{
						command.CommandText = "UPDATE Курс SET курс = @value WHERE дата = @date AND валюта = @valute";
					}
					reader.Close();
					command.ExecuteNonQuery();
				}
			}
		}


		//Загрузка курса всех валют с сайта ЦБ
		private void DownloadCourse()
		{
			WebClient client = new WebClient();
			var xml = client.DownloadString("http://www.cbr.ru/scripts/XML_daily.asp?date_req=" + DateTime.Today.ToShortDateString());
			cb_course = XDocument.Parse(xml);	
		}

		//Получение курса указанной валюты из загруженного ранее курса всех валют
		decimal GetValueNominal(DateTime dat, string ID)
		{
			var elements = cb_course.Element("ValCurs").Elements("Valute");
			return Decimal.Parse(elements.Where(x => x.Attribute("ID").Value == ID).Select(x => x.Element("Value").Value).FirstOrDefault().ToString());		
		}


		private void btShowDayCourse_Click(object sender, EventArgs e)
		{
			MessageBox.Show(GetCourseFromDay(dtpCourseDate.Value, cmbValutes.SelectedItem.ToString()).ToString());
		}
	}
}
