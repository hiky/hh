﻿namespace CBMonitor
{
	partial class frMain
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.dgvTest = new System.Windows.Forms.DataGridView();
			this.dtpCourseDate = new System.Windows.Forms.DateTimePicker();
			this.btShowDayCourse = new System.Windows.Forms.Button();
			this.cmbValutes = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.dgvTest)).BeginInit();
			this.SuspendLayout();
			// 
			// dgvTest
			// 
			this.dgvTest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvTest.Location = new System.Drawing.Point(12, 29);
			this.dgvTest.Name = "dgvTest";
			this.dgvTest.RowTemplate.Height = 24;
			this.dgvTest.Size = new System.Drawing.Size(632, 385);
			this.dgvTest.TabIndex = 0;
			// 
			// dtpCourseDate
			// 
			this.dtpCourseDate.Location = new System.Drawing.Point(733, 108);
			this.dtpCourseDate.Name = "dtpCourseDate";
			this.dtpCourseDate.Size = new System.Drawing.Size(200, 22);
			this.dtpCourseDate.TabIndex = 2;
			// 
			// btShowDayCourse
			// 
			this.btShowDayCourse.Location = new System.Drawing.Point(794, 153);
			this.btShowDayCourse.Name = "btShowDayCourse";
			this.btShowDayCourse.Size = new System.Drawing.Size(75, 23);
			this.btShowDayCourse.TabIndex = 3;
			this.btShowDayCourse.Text = "Show";
			this.btShowDayCourse.UseVisualStyleBackColor = true;
			this.btShowDayCourse.Click += new System.EventHandler(this.btShowDayCourse_Click);
			// 
			// cmbValutes
			// 
			this.cmbValutes.FormattingEnabled = true;
			this.cmbValutes.Items.AddRange(new object[] {
            "BYR",
            "USD",
            "EUR"});
			this.cmbValutes.Location = new System.Drawing.Point(729, 62);
			this.cmbValutes.Name = "cmbValutes";
			this.cmbValutes.Size = new System.Drawing.Size(204, 24);
			this.cmbValutes.TabIndex = 4;
			// 
			// frMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(962, 450);
			this.Controls.Add(this.cmbValutes);
			this.Controls.Add(this.btShowDayCourse);
			this.Controls.Add(this.dtpCourseDate);
			this.Controls.Add(this.dgvTest);
			this.Name = "frMain";
			this.Text = "CBMontior";
			this.Load += new System.EventHandler(this.frMain_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgvTest)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dgvTest;
		private System.Windows.Forms.DateTimePicker dtpCourseDate;
		private System.Windows.Forms.Button btShowDayCourse;
		private System.Windows.Forms.ComboBox cmbValutes;
	}
}

