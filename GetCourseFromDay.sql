﻿USE [CBMonitor]
GO
/****** Object:  UserDefinedFunction [dbo].[GetValue]    Script Date: 30.01.2020 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetValue]
(
  @C_DATE DATE,
  @VALUTE NVARCHAR(3)
)
RETURNS DECIMAL (16,6)
AS
BEGIN
  DECLARE @C_VALUE DECIMAL (16,6)
  SELECT @C_VALUE = курс FROM CBMonitor.dbo.Курс WHERE валюта = @VALUTE AND дата = @C_DATE
  IF @C_VALUE >= 0
	BEGIN
		RETURN @C_VALUE
  END
		RETURN -1
END
